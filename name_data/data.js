
let name_sets = {};

name_sets['egyptian_gods'] = require('./egyptian_gods');
name_sets['egyptian_set'] = require('./egyptian_set');
name_sets['greek_gods'] = require('./greek_gods');
name_sets['greeks'] = require('./greeks');
name_sets['japanese_gods'] = require('./japanese_gods');
name_sets['native_american_gods'] = require('./native_american_gods');

module.exports = name_sets;